use color_backtrace::{default_output_stream, BacktracePrinter};
use colored::*;
use std::process::Command;
use std::io::{self, prelude::*};
use anyhow::*;
use std::fs::{File, read_dir};
use std::io::{BufRead, BufReader};
use toml;
use serde_derive::*;
use std::path::Path;
use std::fs::create_dir_all;

#[macro_use]
extern crate clap;

fn qexe(sh_: &str) -> Result<std::process::Output>{
    let mut sh = sh_.split(" ");
    let mut cmd = Command::new(sh.next().unwrap());
    sh.for_each(|arg|{cmd.arg(arg);});
    cmd.output().context(format!("Failed to run {}",sh_).red())
}
fn exe(sh_: &str) -> Result<std::process::ExitStatus>{
    let mut sh = sh_.split(" ");
    let mut cmd = Command::new(sh.next().unwrap());
    sh.for_each(|arg|{cmd.arg(arg);});
    cmd.status().context(format!("Failed to run {}",sh_).red())
}

#[derive(Clone, Debug)]
struct PKGBUILD{
    depends: Vec<String>,
    makedepends: Vec<String>,
    updated_file: Vec<String>,
}

impl PKGBUILD{
    fn parse(path: &str) -> Result<Self>{
        let mut tmp = Self{
            depends:      Vec::new(),
            makedepends:  Vec::new(),
            updated_file: Vec::new(),
        };

        let mut recording = [false, false];
        let mut buffer = vec![];

        'lines: for line in BufReader::new(File::open(path)?).lines(){
            let mut line = line.unwrap();

            line = line.replace("\t"," ");
            while line.contains("  "){
                line = line.replace("  "," ");
            }

            let mut recordme = &line[..];
            let data = line.split("=").collect::<Vec<_>>();
            let var_name = data[0].replace(" ", "");

            if !(recording[0] || recording[1]) && matches!(&var_name[..], "depends" | "makedepends"){
                if !data[1].contains("("){return Err(anyhow!("Error while parsing: Did not find start of array in PKGBUILD"))}
                match &var_name[..]{
                    "depends" => {
                        recording[0] = true;
                    },
                    "makedepends" => {
                        recording[1] = true;
                    },
                    _ => {continue 'lines}
                }
                recordme = data[1].clone();
            }

            if recording[1] && recording[0]{
                return Err(anyhow!("Unabel to record depends and makedepends at the same time"));
            }
            if !(recording[1] || recording[0]){
                tmp.updated_file.push(line);
                continue 'lines
            }

            'bytes: for b in recordme.bytes(){
                if buffer.len() > 0 && matches!(b as char, ')' | ' '){
                    if recording[0]{
                        &mut tmp.depends
                    }else{
                        &mut tmp.makedepends
                    }.push(String::from_utf8(buffer.clone())?);
                    buffer = vec![];
                }
                match b as char{
                    '(' => {},
                    '\'' => {},
                    ')' => {
                        recording[1] = false;
                        recording[0] = false;
                        buffer = vec![];
                        break 'bytes;
                    },

                    ' ' => {},

                    c => buffer.push(c as u8),
                }
            }

        }
        Ok(tmp)
    }
}

fn handle_deps(su: &str, op: &str, depends: &Vec<String>, hush: bool) -> Result<()>{
    'dep_cycle: for (n, dep) in depends.iter().enumerate(){
        let dep = dep.split(">").next().unwrap();
        println!("({}/{}) apk {} {}", n+1, depends.len(), op, dep);
        let mut res = qexe(&format!("{} apk {} {}", su, op, dep))?;
        if let Some(exit_code) = res.status.code(){
            if exit_code == 0{
                continue 'dep_cycle
            }
            if exit_code == 1{
                if hush{
                    println!("{}","Not found".yellow());
                    continue 'dep_cycle
                }
                println!("\nMaybe try:");
                exe(&format!("apk search {}", dep))?;
                println!(
                    "\r{}: unable to find installation candidate for {}. Please type name of package containing dependecy, or press enter to skip.",
                    "Warning".bold().yellow(), dep
                );
                print!(":");
                io::stdout().flush()?;
                let mut pkg = "".to_string();
                io::stdin().read_line(&mut pkg)?;

                if pkg=="\n"{
                    continue 'dep_cycle
                }

                res = qexe(&format!("{} apk {} {}", su, op, pkg.replace("\n","")))?;
                if res.status.code()==None{
                    continue 'dep_cycle
                }
            }
            return Err(anyhow!(String::from_utf8(res.stderr)?.red())).context(format!("Failed to install dependency {}", dep).red())
        };
    }
    Ok(())
}

#[derive(Deserialize)]
struct Config{
    delete_make_depends: Option<bool>,
    build_dir: String,
    sudo_command: String,
}

fn main() -> Result<()>{
    BacktracePrinter::new().message(format!("{}","Archpk crahsed".blue())).install(default_output_stream());
    let args = clap_app!(Archpk =>
        (version: "0.1.0")
        (author:  "unic0rn9k")
        (about:   "Lets you use pacman and yay on alpine linux")
        (@arg SYNC : -S --sync +takes_value "Package to sync (install)")
        (@arg CONF : -c --config +takes_value "Config file to use. Default is /etc/archpk.toml")
        (@arg HUSH : -n --noconfirm "Dont take user input")
        (@arg GIT  : -g --git "Use git instead of yay (will work for less packages)")
        (@arg FORCE: --force "Overwrite files if they already exist")
        (@arg TRASH: -t --trash "Dont remove temperary files after build")

        (@arg MAKEPKG : -m --makepkg +takes_value "Extra arguments to pass to makepkg")
    ).get_matches();

    let config: Config = {
        let mut config = "/etc/archpk.toml";
        if let Some(conf) = args.value_of("CONF"){
            config = conf
        }
        let mut config_raw = "".to_string();
        let err = format!("Unable to open config file: {}", config).red();

        File::open(config)
            .context(err.clone())?
            .read_to_string(&mut config_raw)
            .context(err.clone())?;

        toml::from_str(&config_raw).context("Malformed config file".red())?
    };

    if let Some(package) = args.value_of("SYNC"){
        let build_dir = format!("{}/{}", config.build_dir, package);
        if Path::new(&build_dir).exists(){
            return Err(anyhow!(
                    r"Build dir '{}' exists.
You can safely remove it if you are sure you arent already trying to install the package",
                    build_dir.bold()
            ));
        }
        create_dir_all(&config.build_dir).context("Failed to create build dir")?;

        println!("{}", format!("Syncing {}...", package).blue().bold());

        if args.is_present("GIT"){
            exe(&format!("git clone https://aur.archlinux.org/{}.git {}", package, build_dir))?;
        }else{
            Command::new("yay").arg("-G")
                .arg(package)
                .current_dir(&config.build_dir)
                .status().context("Failed to call yay".red())?;
        }

        let pkg = PKGBUILD::parse(&format!("{}/PKGBUILD", build_dir)).context("Failed to parse PKGBUILD".red())?;
        handle_deps(&config.sudo_command, "add", &pkg.depends,     args.is_present("HUSH"))?;
        handle_deps(&config.sudo_command, "add", &pkg.makedepends, args.is_present("HUSH"))?;

        File::create(&format!("{}/PKGBUILD", build_dir))?.write_all(
            &pkg.updated_file.join("\n").bytes().collect::<Vec<_>>()[..]
        ).context(format!("Failed to write to '{}/PKGBUILD'",build_dir).red())?;

        Command::new("makepkg").arg(
            if let Some(arg) = args.value_of("MAKEPKG"){format!("-{}",arg)}else{String::new()}
        ).current_dir(&build_dir).status()?;

        if matches!(config.delete_make_depends, Some(b) if b){
            handle_deps(&config.sudo_command, "del -r", &pkg.makedepends, args.is_present("HUSH"))?;
        }

        for file in read_dir(&build_dir)?{
            let file = format!("{}", file?.path().display());
            if file.contains(".pkg.tar.gz"){
                exe(&format!("{} pacman -U --noconfirm {}{}",
                    config.sudo_command,
                    if args.is_present("FORCE"){"--overwrite "}else{""},
                    file,
                )).context("Failed to call pacman")?;
            }
        }

        if !args.is_present("TRASH"){
            exe(&format!("rm -rf {}", build_dir))?;
        }
    }
    Ok(())
}
