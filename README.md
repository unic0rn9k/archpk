# The Alpine AUR Helper

## Installation
I am going to realease an installation script at some point, but for now you are going to have to figure it out yourself.

## Configuration
config file is located in '/etc/archpk.toml'

my config:
<pre>
build_dir = "/tmp/archpk"
sudo_command = "doas"
</pre>

## Known packages that work
 - desmume ^10/10^ (Only cli works, but it can play games, I guess thats all it needs to do realy)
 - pdfjs ^10/10^
 - yay ^7/10^
 - blueman ^2/10^ (Mby)
 - webtorrent-cli ^10/10^ (Desktop version doesnt work, but cli can stream to mpv, so who cares)
 - cordless-git ^10/10^
 - citra-git ^10/10^
 - hcxtools-git
 - bully (needs libpcap-dev)
 - wifite2-git
 - hcxdumptool-git
 - pyrit-git (needs python2-dev and python3-dev)
 - hashcat-git (needs xxhash-dev and findutils)

## Notes
Keep in mind some packages will need you to manually install some dependencies.
Also i likely won't update in the future, since i changed distro to artix.

## Basic Usage
use -S to for a general install, maybe use -gS for git versions instead
